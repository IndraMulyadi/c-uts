#include <iostream>

using namespace std;

// Fungsi untuk menghitung faktorial
int hitungFaktorial(int x) {
    if (x == 0 || x == 1) {
        return 1;
    } else {
        return x * hitungFaktorial(x - 1);
    }
}

// Prosedur untuk menghitung permutasi
void permutasi() {
    int x, y;
    cout << "Masukkan nilai x: ";
    cin >> x;
    cout << "Masukkan nilai y: ";
    cin >> y;

    if (x >= y && x >= 0 && y >= 0) {
        int hasilPermutasi = hitungFaktorial(x) / hitungFaktorial(x - y);
        cout << "Permutasi dari " << x << " dan " << y << " adalah: " << hasilPermutasi << endl;
    } else {
        cout << "Masukan tidak valid untuk permutasi." << endl;
    }
}

// Prosedur untuk menghitung kombinasi
void kombinasi() {
    int x, y;
    cout << "Masukkan nilai x: ";
    cin >> x;
    cout << "Masukkan nilai y: ";
    cin >> y;

    if (x >= y && x >= 0 && y >= 0) {
        int hasilKombinasi = hitungFaktorial(x) / (hitungFaktorial(y) * hitungFaktorial(x - y));
        cout << "Kombinasi dari " << x << " dan " << y << " adalah: " << hasilKombinasi << endl;
    } else {
        cout << "Masukan tidak valid untuk kombinasi." << endl;
    }
}

int main() {
    int pilihan;

    do {
        // Menampilkan menu
        cout << "Menu:\n";
        cout << "1. Hitung Permutasi\n";
        cout << "2. Hitung Kombinasi\n";
        cout << "0. Keluar\n";
        cout << "Pilih menu (0/1/2): ";
        cin >> pilihan;

        // Memproses pilihan
        switch (pilihan) {
            case 1:
                permutasi();
                break;
            case 2:
                kombinasi();
                break;
            case 0:
                cout << "Terima kasih. Program selesai.\n";
                break;
            default:
                cout << "Pilihan tidak valid. Silakan coba lagi.\n";
                break;
        }

    } while (pilihan != 0);

    return 0;
}
