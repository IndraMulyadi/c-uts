// gaji.c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "gaji.h"

void jdl_aplikasi() {
    printf("*********\n");
    printf("*  Aplikasi Hitung Gaji *\n");
    printf("*********\n\n");
}

void gapok_tunja(char gol, char *status, float *gapok, float *tunja) {
    if (gol == 'A') {
        *gapok = 200000;
        if (strcmp(status, "Nikah") == 0) {
            *tunja = 50000;
        } else if (strcmp(status, "Belum") == 0) {
            *tunja = 25000;
        }
    } else if (gol == 'B') {
        *gapok = 350000;
        if (strcmp(status, "Nikah") == 0) {
            *tunja = 75000;
        } else if (strcmp(status, "Belum") == 0) {
            *tunja = 60000;
        }
    }
}

float prosen_potongan(float gapok) {
    float prosen_pot = 0.0;

    if (gapok > 300000) {
        prosen_pot = 0.1;
    }

    return prosen_pot;
}

float potongan(float gapok, float tunja, float prosen_pot) {
    return (gapok + tunja) * prosen_pot;
}

float gaji_bersih(float gapok, float tunja, float pot) {
    return (gapok + tunja) - pot;
}

void input(char *nama, char *gol, char *status) {
    printf("Nama Karyawan       : ");
    scanf("%s", nama);
    printf("Golongan (A\\B)      : ");
    scanf(" %c", gol);
    printf("Status (Nikah\\Belum): ");
    scanf("%s", status);
}

void output(float gapok, float tunja, float pot, float gaber) {
    printf("Gaji Pokok     : Rp.%.2f\n", gapok);
    printf("Tunjangan      : Rp.%.2f\n", tunja);
    printf("Potongan Iuran : Rp.%.2f\n", pot);
    printf("Gaji Bersih    : Rp.%.2f\n", gaber);
}
