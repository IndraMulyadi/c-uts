#include "gaji.h"

int main() {
    system("clear");

    char nama[50];
    char gol = ' ';
    char status[10];

    float gapok = 0.0;
    float gaber = 0.0;
    float tunja = 0.0;
    float pot = 0.0;
    float prosen_pot = 0.05;

    jdl_aplikasi();
    input(nama, &gol, status);
    gapok_tunja(gol, status, &gapok, &tunja);

    switch (gol) {
        case 'A':
            gapok = 200000;
            if (strcmp(status, "Nikah") == 0) {
                tunja = 50000;
            } else {
                if (strcmp(status, "Belum") == 0) {
                    tunja = 25000;
                }
            }
            break;
        case 'B':
            gapok = 350000;
            if (strcmp(status, "Nikah") == 0) {
                tunja = 75000;
            } else {
                if (strcmp(status, "Belum") == 0) {
                    tunja = 60000;
                }
            }
            break;
        default:
            printf(" Salah Input Golongan !!!\n");
            return 0;
    }

    if (gapok > 300000) {
        prosen_pot = 0.1;
    }

    pot = (gapok + tunja) * prosen_pot;
    gaber = (gapok + tunja) - pot;

    printf("Gaji Pokok     : Rp.%.2f\n", gapok);
    printf("Tunjangan      : Rp.%.2f\n", tunja);
    printf("Potongan Iuran : Rp.%.2f\n", pot);
    printf("Gaji Bersih    : Rp.%.2f\n", gaber);

    return 0;
}
