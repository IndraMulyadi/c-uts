#include <iostream>

using namespace std;

int faktorial(int n) {
    if (n == 0 || n == 1) {
        return 1;
    } else {
        return n * faktorial(n - 1);
    }
}

int kombinasi(int n, int r) {
    if (r > n) {
        return 0; 
    } else {
        return faktorial(n) / (faktorial(r) * faktorial(n - r));
    }
}

int main() {
    int tingkat;

    cout << "Masukkan N: ";
    cin >> tingkat; 

    for (int i = 0; i < tingkat; i++) {
        for (int j = 0; j < tingkat - i - 1; j++) {
            cout << " "; 
        }

        for (int j = 0; j <= i; j++) {
            cout << kombinasi(i,j)<<" ";  
        }

        cout << endl; 
    }

    return 0;
}
